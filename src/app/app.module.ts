import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuotesComponent } from './quotes/quotes.component';
import { QuotesBlockComponent } from './quotes/quotes-block/quotes-block.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { QuotesNavComponent } from './quotes/quotes-nav/quotes-nav.component';
import { NotFoundComponent } from './not-found.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { EditQuoteComponent } from './edit-quote/edit-quote.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    QuotesComponent,
    QuotesBlockComponent,
    NavbarComponent,
    QuotesNavComponent,
    NewQuoteComponent,
    EditQuoteComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
