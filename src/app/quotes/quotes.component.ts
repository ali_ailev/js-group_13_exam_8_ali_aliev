import { Component, OnInit } from '@angular/core';
import { Quote } from '../shared/quote.model';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {

  quotes: Quote[] = [];
  category: string = '';
  url: string = '';


  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<{ [id: string]: Quote }>('https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(
            id,
            quoteData.name,
            quoteData.text,
            quoteData.category
          )
        })
      })).subscribe(quotes => {
      this.quotes = quotes;
    });

  }

  getCategory(category: string) {
    this.category = category;
    console.log(this.category);
    switch (this.category) {
      case '/':
        this.url = 'https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes.json';
        break;
      default:
        this.url = `https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="${this.category}"`;
        break;
    }
    this.http.get<{ [id: string]: Quote }>(this.url)
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(
            id,
            quoteData.name,
            quoteData.text,
            quoteData.category
          )
        })
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
      });
  }


}
