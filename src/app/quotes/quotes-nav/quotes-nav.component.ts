import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-quotes-nav',
  templateUrl: './quotes-nav.component.html',
  styleUrls: ['./quotes-nav.component.css']
})
export class QuotesNavComponent {
  @Output() category = new EventEmitter();

  getCategory(category: string) {
    this.category.emit(category);
  }
}
