import { Component, Input, OnInit } from '@angular/core';
import { Quote } from '../../shared/quote.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quotes-block',
  templateUrl: './quotes-block.component.html',
  styleUrls: ['./quotes-block.component.css']
})
export class QuotesBlockComponent implements OnInit{
  @Input() quote!: Quote;
  quoteId:any;
  constructor(private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.quoteId = this.route.snapshot.params['id'];
    console.log(this.quoteId);
  }

  deleteQuote() {
    this.http.delete('https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes/' + this.quote.id + '.json')
      .subscribe();
    this.http.get<{ [id: string]: Quote }>("https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes.json")
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(
            id,
            quoteData.name,
            quoteData.text,
            quoteData.category
          )
        })
      }))
      .subscribe();
  }

}
