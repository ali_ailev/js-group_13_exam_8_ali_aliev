import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotesComponent } from './quotes/quotes.component';
import { NotFoundComponent } from './not-found.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { EditQuoteComponent } from './edit-quote/edit-quote.component';

const routes: Routes = [
  {path: '', component: QuotesComponent},
  {path: 'quotes', component: QuotesComponent,children:[
      {path:'motivation',component:QuotesComponent},
      {path:'witcher',component:QuotesComponent},
      {path:'auf',component:QuotesComponent},
      {path:'starWars',component:QuotesComponent},
      {path:'humor',component:QuotesComponent},
    ]},
  {path: 'create', component: NewQuoteComponent},
  {path: 'edit', component: EditQuoteComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
