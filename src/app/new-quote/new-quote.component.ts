import { Component, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent {
  @ViewChild('nameInput')nameInput!:ElementRef;
  @ViewChild('categorySelect')categorySelect!:ElementRef;
  @ViewChild('quoteText')quoteText!:ElementRef;


  constructor(public http: HttpClient) {
  }

  createQuote() {
    const name = this.nameInput.nativeElement.value;
    const category = this.categorySelect.nativeElement.value;
    const text = this.quoteText.nativeElement.value;
    const post = {name,text,category};

    this.http.post('https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes.json',post)
      .subscribe();
  }
}
