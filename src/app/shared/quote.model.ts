export class Quote {
  constructor(
    public id: string,
    public name: string,
    public text: string,
    public category: string,
  ) {
  }
}
