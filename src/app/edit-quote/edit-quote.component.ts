import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { Quote } from '../shared/quote.model';

@Component({
  selector: 'app-edit-quote',
  templateUrl: './edit-quote.component.html',
  styleUrls: ['./edit-quote.component.css']
})
export class EditQuoteComponent implements OnInit {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('categorySelect') categorySelect!: ElementRef;
  @ViewChild('quoteText') quoteText!: ElementRef;

  quote!:Quote;

  constructor(private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const quoteId = parseInt(params['id']);
      console.log(quoteId);
    });
  }

  editQuote() {
    const name = this.nameInput.nativeElement.value;
    const category = this.categorySelect.nativeElement.value;
    const text = this.quoteText.nativeElement.value;
    const put = {name, text, category};

    this.http.put('https://js-group-13-exam-8-ali-aliev-default-rtdb.firebaseio.com/quotes.json', put)
      .subscribe();
  }
}
